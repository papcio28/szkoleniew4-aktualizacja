package com.android.szkolenie.szkoleniew4_lokalizacja;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashSet;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class MainActivity extends ActionBarActivity implements LocationListener {

    @InjectView(R.id.szczegoly_lokalizacji)
    protected TextView mText;

    // Komponent do obsługi lokalizacji
    private LocationManager mLocationManager;

    // Zbiór aktywnych w danym momencie providerów
    private HashSet<String> mEnabledProviders = new HashSet<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Na zakończenie aktywności wyrejestrowanie się z pomiarów lokalizacji
        // Jeżeli tego nie zrobimy to telefon może non stop nasłuchiwać na GPS - BATERIA !!!!
        mLocationManager.removeUpdates(this);
    }

    // Ogólny kod pobierający ostatnią lokalizacją, przyjmuje jako parametr provider lokalizacji.
    public void pobierzOstatnia(String locationProvider) {
        if(!mLocationManager.isProviderEnabled(locationProvider)) {
            Toast.makeText(this,
                    "Włącz "+ locationProvider+ " w ustawieniach lokalizacji !",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        Location mLastLocation =
                mLocationManager.getLastKnownLocation(locationProvider);
        if(mLastLocation != null) mText.setText(mLastLocation.toString());
        else Toast.makeText(this, "Brak ostatniej lokalizacji "+locationProvider+" !",
                Toast.LENGTH_SHORT).show();
    }

    // Ogólny kod startujący nasłuchiwanie na konkretnym providerze
    public void nasluchStart(String provider) {
        if(!mLocationManager.isProviderEnabled(provider)) {
            Toast.makeText(this,
                    "Włącz "+ provider+ " w ustawieniach lokalizacji !",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        // 2*1000 - sekundy, *1000 dlatego ze czas musi byc wyrazony w milisekundach
        mLocationManager.requestLocationUpdates(provider, 2*1000, 0, this);
        mEnabledProviders.add(provider);    // Dodajemy włączany provider do listy aktywnych
    }

    public void nasluchStop(String provider) {
        if(!mLocationManager.isProviderEnabled(provider)) {
            Toast.makeText(this,
                    "Włącz "+ provider+ " w ustawieniach lokalizacji !",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        // Nasluchiwanie wyrejestrowuje caly Listener, od wszystkich Providerow
        mLocationManager.removeUpdates(this);
        mEnabledProviders.remove(provider);     // Usunięcie stopowanego providera z listy aktywnych

        // Ponieważ funkcja removeUpdates usuwa aktualizacji ze wszystkich providerów,
        // iterujemy po kolekcji pozostałych aktywnych providerów i włączamy dla nich nasłuch.
        for(String mProvider : mEnabledProviders) {
            nasluchStart(mProvider);
        }
    }

    // Pobierz ostanią znaną lokalizacje GPS
    @OnClick(R.id.gps_ostatnia)
    public void pobierzOstatniaGPS() {
        pobierzOstatnia(LocationManager.GPS_PROVIDER);
    }

    // Pobierz ostanią znaną lokalizacje GSM
    @OnClick(R.id.gsm_ostatnia)
    public void pobierzOstatniaGSM() {
        pobierzOstatnia(LocationManager.NETWORK_PROVIDER);
    }

    // Rozpocznij nasłuchiwanie na lokalizację GPS
    @OnClick(R.id.gps_start)
    public void nasluchGPSStart() {
        nasluchStart(LocationManager.GPS_PROVIDER);
    }

    // Rozpocznij nasłuchiwanie na lokalizację GSM
    @OnClick(R.id.gsm_start)
    public void nasluchGSMStart() {
        nasluchStart(LocationManager.NETWORK_PROVIDER);
    }

    // Zakończ nasłuch na lokalizację GPS
    @OnClick(R.id.gps_end)
    public void nasluchGPSStop() {
        nasluchStop(LocationManager.GPS_PROVIDER);
    }

    // Zakończ nasłuch na lokalizację GSM
    @OnClick(R.id.gsm_end)
    public void nasluchGSMStop() {
        nasluchStop(LocationManager.NETWORK_PROVIDER);
    }

    // Na przyjście nowej lokalizacji z providera !
    @Override
    public void onLocationChanged(Location location) {
        if(location != null) mText.setText(location.toString());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
